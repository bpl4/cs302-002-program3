#include "header.h"

int menu()
{
	int option = 0;

	cout << "WELCOME TO THE PAWESOME ADOPTIONS\n\n";
	
	cout << "1. Show List of All Pets\n"\
		"2. Show List of All Dogs\n"\
		"3. Show List of All Cats\n"\
		"4. Show List of All Fish\n"\
		"5. Exit\n";
	
	cin >> option;
	cin.ignore(100, '\n');
	return option;
}

void backend(Pet & mypets)
{
	bool program = true;
	int option = 0;

	while (program)
	{
		cout << "BACKEND MENU\n\n";

		cout << "1. Add Dog\n"
			"2. Add Cat\n"
			"3. Add Fish\n"
			"4. Generate a Test List\n"
			"5. Exit\n";

		cin >> option;
		cin.ignore(100, '\n');

		switch(option)
		{
			case 1:
				{
					int id = 0;
					string name;
					string breed;
					float weight;
					int care_level = 0;
					char temp_input;
					bool temperament;
					string characteristic;
					string history;

					cout << "\nEnter the Dog's Microchip ID: ";
					cin >> id;
					cin.ignore(100, '\n');

					cout << "\nEnter the Dog's name: ";
					getline(cin, name);
					
					cout << "\nEnter the Dog's breed: ";
					getline(cin, breed);

					cout << "\nEnter the weight (lbs): ";
					cin >> weight;
					cin.ignore(100, '\n');

					cout << "\nEnter the Level of Care.\n(1 = Little Care Needed | 2 = Moderate Care Needed | 3 = Lots of Care Needed)\n";;
					cin >> care_level;
					cin.ignore(100, '\n');

					cout << "\nIs this Pet aggressive? (Y/N): ";
					cin >> temp_input;
					cin.ignore(100, '\n');

					if (temp_input == 'y' || temp_input == 'Y')
						temperament = true;
					else
						temperament = false;
					
					cout << "\nEnter the Pet's Characteristics/Features/Description\n";
					getline(cin, characteristic);

					cout << "\nEnter this Pet's History\n";
					getline(cin, history);

					Dog mydog(id,  name, breed, weight, care_level, temperament, characteristic, history);	
					mypets.insert(mydog);
					break;
				}

			case 2:
				{
					string personality;
					string name;
					string breed;
					float weight;
					int care_level = 0;
					char temp_input;
					bool temperament;
					string characteristic;
					string history;

					cout << "\nEnter the Personality of this Cat\n";
					getline(cin, personality);

					cout << "\nEnter the Cat's name: ";
					getline(cin, name);
					
					cout << "\nEnter the Cat's breed: ";
					getline(cin, breed);

					cout << "\nEnter the weight (lbs): ";
					cin >> weight;
					cin.ignore(100, '\n');

					cout << "\nEnter the Level of Care.\n(1 = Little Care Needed | 2 = Moderate Care Needed | 3 = Lots of Care Needed)\n";;
					cin >> care_level;
					cin.ignore(100, '\n');

					cout << "\nIs this Pet aggressive? (Y/N): ";
					cin >> temp_input;
					cin.ignore(100, '\n');

					if (temp_input == 'y' || temp_input == 'Y')
						temperament = true;
					else
						temperament = false;
					
					cout << "\nEnter the Pet's Characteristics/Features/Description\n";
					getline(cin, characteristic);

					cout << "\nEnter this Pet's History\n";
					getline(cin, history);


					Cat mycat(personality, name, breed, weight, care_level, temperament, characteristic, history);
					mypets.insert(mycat);
					break;
				}

			case 3:
				{
					string species;
					string compatibility;
					string name;
					string breed;
					float weight;
					int care_level = 0;
					char temp_input;
					bool temperament;
					string characteristic;
					string history;

					cout << "\nEnter the Species of this Fish\n";
					getline(cin, species);
					
					cout << "\nWhat fishes are they compatible/incompatible with?\n";
					getline(cin, compatibility);

					cout << "\nEnter the Fish's name: ";
					getline(cin, name);

					breed = species;

					cout << "\nEnter the weight (lbs): ";
					cin >> weight;
					cin.ignore(100, '\n');

					cout << "\nEnter the Level of Care.\n(1 = Little Care Needed | 2 = Moderate Care Needed | 3 = Lots of Care Needed)\n";;
					cin >> care_level;
					cin.ignore(100, '\n');

					cout << "\nIs this Pet aggressive? (Y/N): ";
					cin >> temp_input;
					cin.ignore(100, '\n');

					if (temp_input == 'y' || temp_input == 'Y')
						temperament = true;
					else
						temperament = false;
					
					cout << "\nEnter the Pet's Characteristics/Features/Description\n";
					getline(cin, characteristic);

					cout << "\nEnter this Pet's History\n";
					getline(cin, history);

					Fish myfish(species, compatibility, name, breed, weight, care_level, temperament, characteristic, history);
					mypets.insert(myfish);
					break;

				}

			case 4:
				test_list(mypets);
				break;

			case 5:
				program = false;
				break;

			default:
				cout << "Not an option, please try again\n\n";
				break;
				
		}
	}
}

void test_list(Pet & mypets)
{
	//cout << "ADDING A PET TO THE TREE\n";
	Cat mycat("A very independent cat", "Caro", "Ragdoll", 10, 1, true, "Orange", "Loves to be petted");
	mypets.insert(mycat);
	//cout << mypets.display() << " pets total\n\n\n\n";

	Cat mycat2("A very independent cat", "Maro", "British Longhair", 10, 1, true, "Orange, Black, and White", "Loves to be petted");
	mypets.insert(mycat2);


	//cout << "ADDING A PET TO THE TREE\n";
	Dog mydog2(420, "Bob", "Lab", 120, 3, false, "Caring", "Loves Treats");	
	mypets.insert(mydog2);
	//cout << mypets.display() << " pets total\n\n\n\n"; 

	//cout << "ADDING A PET TO THE TREE\n";
	Dog mydog(420, "Alex", "Husky", 115, 3, false, "Stubborn", "Loves Snow");	
	mypets.insert(mydog);
	//cout << mypets.display() << " pets total\n\n\n\n";

	//cout << "ADDING A PET TO THE TREE\n";
	Dog mydog3(420, "Dodger", "Pitbull", 95, 3, true, "Protective", "Will bite");	
	mypets.insert(mydog3);
	//cout << mypets.display() << " pets total\n\n\n\n";

	//cout << "ADDING A PET TO THE TREE\n";
	Dog mydog4(420, "Crasher", "Pitbull", 98, 3, true, "Protective", "Will roll around");	
	mypets.insert(mydog4);
	//cout << mypets.display() << " pets total\n\n\n\n";

	//cout << "ADDING A PET TO THE TREE\n";
	Dog mydog5(420, "Dasher", "Chihuahua", 10, 3, true, "Petty", "Get ready to go deaf");	
	mypets.insert(mydog5);
	//cout << mypets.display() << " pets total\n\n\n\n";
	
	//cout << "ADDING A PET TO THE TREE\n";
	Fish myfish("Goldfish", "Best kept with passive fish", "Sage", "N/A", 0.5, 2, false, "Golden scales", "Very active");
	mypets.insert(myfish);
	//cout << mypets.display() << " pets total\n\n\n\n";

	//mypets.weight_filter(100);
	//mypets.cats_filter();
	//mypets.fish_filter();

}
