#include "header.h"
/* Brian Le

   This file contains the implementations of the base and derived class's member functions
   */

// Animals
Animal::Animal(): level_of_care(0), weight(0.0), temperament(false)
{}

Animal::Animal(const string & name, const string & breed, float weight, int level_of_care, bool temperament, const string & physical_characteristics,\
		const string & history): name(name), breed(breed), level_of_care(level_of_care), weight(weight), temperament(temperament), physical_characteristics(physical_characteristics),\
					 history(history)
{
}

Animal::~Animal()
{
}

bool Animal::weight_check(float weight) const
{
	// if the weight that the user wants is less or equal than the pet's weight, return true.
	return this->weight <= weight;
}

bool Animal::temperament_check(bool temperament) const
{
	return this->temperament == temperament;
}

bool Animal::care_check(int care) const // check level of care
{
	return level_of_care == care;	
}

int Animal::name_compare(const Animal & src) const
{
	return this->name.compare(src.name);
}


/*
bool Animal::operator< (const Animal & src) const
{
	return this->name < src.name;
}

bool Animal::operator<= (const Animal & src) const
{}

bool Animal::operator> (const Animal & src) const
{}

bool Animal::operator>= (const Animal & src) const
{}

bool Animal::operator== (const Animal & src) const
{}
*/ // will be using string compare() instead to return the difference.


/*
void Animal::display() const
{}// all derives must have this implemented. Some need to print different infos depending on the pet type.
*/


// Dogs
Dog::Dog()
{}

Dog::Dog(int id, const string & name, const string & breed, float weight, int level_of_care, bool temperament, const string & physical_characteristics,\
		const string & history): Animal(name, breed, weight, level_of_care, temperament, physical_characteristics, history), microchip_id(id)
{}

Dog::~Dog()
{}


bool Dog::operator== (const Dog & src) const
{
	return this->microchip_id == src.microchip_id;
}


void Dog::display() const
{
	cout << "\nName: " << name << endl;
	cout << "Animal: Dog\n";
	cout << "Breed: " << breed << endl;
	cout << "Weight (lbs): " << weight << endl;
	cout << "Level of Care: ";

	switch (level_of_care)
	{
		case 1:
			cout << "Pet is mostly independent. Easy to take care\n";
			break;
		case 2:
			cout << "Pet needs moderate care and attention\n";
			break;
		case 3:
			cout << "Pet needs a lot of care and attention\n";
			break;
		default:
			cout << "N/A\n";
			break;
	}

	if (temperament)
		cout << "Temperament: Aggressive" << endl;
	else
		cout << "Temperament: Passive" << endl;

	cout << "Physical Characteristics: " << physical_characteristics << endl;
	cout << "History: " << history << endl;
	cout << '\n';

}

bool Dog::weight_check(float weight) const
{
	return this->weight <= weight;
}

bool Dog::temperament_check(bool temperament) const
{
	return temperament == this->temperament;
}

bool Dog::care_check(int care) const
{
	return care <= level_of_care;	
}



// Cats
Cat::Cat()
{}

Cat::Cat(const string & personality, const string & name, const string & breed, float weight, int level_of_care, bool temperament, const string & physical_characteristics,\
		const string & history): Animal(name, breed, weight, level_of_care, temperament, physical_characteristics, history), personality(personality)
{}

Cat::~Cat()
{}


void Cat::display() const
{
	cout << "\nName: " << name << endl;
	cout << "Animal: Cat\n";
	cout << "Breed: " << breed << endl;
	cout << "Weight (lbs): " << weight << endl;
	cout << "Level of Care: ";

	switch (level_of_care)
	{
		case 1:
			cout << "Pet is mostly independent. Easy to take care\n";
			break;
		case 2:
			cout << "Pet needs moderate care and attention\n";
			break;
		case 3:
			cout << "Pet needs a lot of care and attention\n";
			break;
		default:
			cout << "N/A\n";
			break;
	}

	if (temperament)
		cout << "Temperament: Aggressive" << endl;
	else
		cout << "Temperament: Passive" << endl;

	cout << "Physical Characteristics: " << physical_characteristics << endl;
	cout << "History: " << history << endl;
	cout << '\n';

}

bool Cat::temperament_check(bool temperament) const
{
	return this->temperament == temperament;
}

bool Cat::care_check(int care) const
{
	return care <= level_of_care;
}

bool Cat::weight_check(float weight) const
{
	return this->weight <= weight;
}



// Fish
Fish::Fish()
{}

Fish::Fish(const string & species, const string & compatibility, const string & name, const string & breed, float weight, \
		int level_of_care, bool temperament, const string & physical_characteristics,\
		const string & history): Animal(name, breed, weight, level_of_care, temperament, physical_characteristics, history), species(species), compatibility(compatibility)
{}

Fish::~Fish()
{}


void Fish::display() const
{
	cout << "\nName: " << name << endl;
	cout << "Animal: Fish\n";
	cout << "Species: " << breed << endl;
	cout << "Level of Care: ";

	switch (level_of_care)
	{
		case 1:
			cout << "Pet is mostly independent. Easy to take care\n";
			break;
		case 2:
			cout << "Pet needs moderate care and attention\n";
			break;
		case 3:
			cout << "Pet needs a lot of care and attention\n";
			break;
		default:
			cout << "N/A\n";
			break;
	}


	cout << "Physical Characteristics: " << physical_characteristics << endl;
	cout << '\n';

}

void Fish::display_compatibility() const
{
	cout << "Notes on compatibility among other fishes:\n" << compatibility << endl;
}

bool Fish::temperament_check(bool temperament) const
{
	return temperament == this->temperament;
}

bool Fish::care_check(int care) const
{
	return care <= level_of_care;
}

bool Fish::weight_check(float weight) const
{
	return this->weight <= weight;
}

