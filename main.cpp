#include "header.h"

int main()
{
	/* Test Block
	{
		Pet mypets;

		//cout << "ADDING A PET TO THE TREE\n";
		Cat mycat("A very independent cat", "Caro", "Ragdoll", 10, 1, true, "Orange", "Loves to be petted");
		mypets.insert(mycat);
		//cout << mypets.display() << " pets total\n\n\n\n";

		Cat mycat2("A very independent cat", "Maro", "British Longhair", 10, 1, true, "Orange, Black, and White", "Loves to be petted");
		mypets.insert(mycat2);


		//cout << "ADDING A PET TO THE TREE\n";
		Dog mydog2(420, "Bob", "Lab", 120, 3, false, "Caring", "Loves Treats");	
		mypets.insert(mydog2);
		//cout << mypets.display() << " pets total\n\n\n\n"; 

		//cout << "ADDING A PET TO THE TREE\n";
		Dog mydog(420, "Alex", "Husky", 115, 3, false, "Stubborn", "Loves Snow");	
		mypets.insert(mydog);
		//cout << mypets.display() << " pets total\n\n\n\n";

		//cout << "ADDING A PET TO THE TREE\n";
		Dog mydog3(420, "Dodger", "Pitbull", 95, 3, true, "Protective", "Will bite");	
		mypets.insert(mydog3);
		//cout << mypets.display() << " pets total\n\n\n\n";

		//cout << "ADDING A PET TO THE TREE\n";
		Dog mydog4(420, "Crasher", "Pitbull", 98, 3, true, "Protective", "Will roll around");	
		mypets.insert(mydog4);
		//cout << mypets.display() << " pets total\n\n\n\n";

		//cout << "ADDING A PET TO THE TREE\n";
		Dog mydog5(420, "Dasher", "Chihuahua", 10, 3, true, "Petty", "Get ready to go deaf");	
		mypets.insert(mydog5);
		//cout << mypets.display() << " pets total\n\n\n\n";
		
		//cout << "ADDING A PET TO THE TREE\n";
		Fish myfish("Goldfish", "Best kept with passive fish", "Sage", "N/A", 0.5, 2, false, "Golden scales", "Very active");
		mypets.insert(myfish);
		//cout << mypets.display() << " pets total\n\n\n\n";

		//mypets.weight_filter(100);
		//mypets.cats_filter();
		//mypets.fish_filter();

	}
	*/


	Pet pet_list_test;
	test_list(pet_list_test);

	Pet mypets;

	bool program = true;

	while (program)
	{
		int option = menu();

		switch (option)
		{
			case -1:
				backend(mypets);
				break;
			case 1:
				mypets.display();
				break;
			case 2:
				mypets.dogs_filter();
				break;
			case 3:
				mypets.cats_filter();
				break;
			case 4:
				mypets.fish_filter();
				break;
			case 5:
				program = false;
				break;
			default:
				cerr << "Not a valid option. Please try again\n";
				break;
		}
	}

	return 0;
}
