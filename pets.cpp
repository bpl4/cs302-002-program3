#include "header.h"
#include "error.h"
/* Brian Le

   This file contains the implementations of the Animal node and the client class
   */


// Animal Node
Animal_Node::Animal_Node(): left(nullptr), right(nullptr), left_isRed(false), right_isRed(false)
{}

Animal_Node::~Animal_Node()
{
	if (animal_ptr)
		delete animal_ptr;

	left.reset();
	right.reset();
}

Animal_Node::Animal_Node(const Animal_Node & src): left(nullptr), right(nullptr), left_isRed(false), right_isRed(false)
{
	// Using RTTI to determine which derived class.
	const Dog * dptr = dynamic_cast<const Dog*> (src.animal_ptr); // Downcasting
	const Cat * cptr = dynamic_cast<const Cat*> (src.animal_ptr);
	const Fish * fptr = dynamic_cast<const Fish*> (src.animal_ptr);

	// create a copy and allocate a new derived object.
	if (dptr)
		animal_ptr = new Dog(*dptr);

	if (cptr)
		animal_ptr = new Cat(*cptr);

	if (fptr)
		animal_ptr = new Fish(*fptr);

	
	/* Let Programmer manually set these flags.
	left_isRed = src.left_isRed;
	right_isRed = src.right_isRed;
	*/
}

Animal_Node::Animal_Node(const Animal & src): left(nullptr), right(nullptr), left_isRed(false), right_isRed(false)
{
	// Using RTTI to determine which derived class.
	const Dog * dptr = dynamic_cast<const Dog*> (&src); // Downcasting
	const Cat * cptr = dynamic_cast<const Cat*> (&src);
	const Fish * fptr = dynamic_cast<const Fish*> (&src);

	// create a copy and allocate a new derived object.
	if (dptr)
		animal_ptr = new Dog(*dptr);

	if (cptr)
		animal_ptr = new Cat(*cptr);

	if (fptr)
		animal_ptr = new Fish(*fptr);

	// Let the programmer manually set the red flags.
}

Animal_Node & Animal_Node::operator=(const Animal_Node & src)
{
	if (animal_ptr)
		delete animal_ptr;

	// Using RTTI to determine which derived class.
	const Dog * dptr = dynamic_cast<const Dog*> (src.animal_ptr); // Downcasting
	const Cat * cptr = dynamic_cast<const Cat*> (src.animal_ptr);
	const Fish * fptr = dynamic_cast<const Fish*> (src.animal_ptr);

	// create a copy and allocate a new derived object.
	if (dptr)
		animal_ptr = new Dog(*dptr);

	if (cptr)
		animal_ptr = new Cat(*cptr);

	if (fptr)
		animal_ptr = new Fish(*fptr);

		

	//left_isRed = src.left_isRed;
	//right_isRed = src.right_isRed;
	return *this;
}

void Animal_Node::set_left(node & new_left)
{
	left = move(new_left);
}

void Animal_Node::set_right(node & new_right)
{
	right = move(new_right);
}


typename Animal_Node::node & Animal_Node::get_left()
{
	return left;
}

typename Animal_Node::node & Animal_Node::get_right()
{
	return right;
}

bool Animal_Node::left_red() const
{
	return left_isRed;
}

bool Animal_Node::right_red() const
{
	return right_isRed;
}

bool Animal_Node::set_left_red(bool flag)
{
	left_isRed = flag;	
	return left_isRed;
}

bool Animal_Node::set_right_red(bool flag)
{
	right_isRed = flag;
	return right_isRed;
}

void Animal_Node::display() const
{
	if (animal_ptr)
		animal_ptr->display();
}

int Animal_Node::compare(const Animal & src)
{
	return animal_ptr->name_compare(src);
}

bool Animal_Node::weight_filter(float weight) const
{
	return animal_ptr->weight_check(weight);
}

bool Animal_Node::is_Dog() const
{
	const Dog * dptr = dynamic_cast<const Dog*> (animal_ptr); 
	
	if (dptr)
		return true;
	return false;
}

bool Animal_Node::is_Cat() const
{
	const Cat * cptr = dynamic_cast<const Cat*> (animal_ptr);

	if (cptr)
		return true;
	return false;
}

bool Animal_Node::is_Fish() const
{
	const Fish * fptr = dynamic_cast<const Fish*> (animal_ptr);

	if (fptr)
		return true;
	return false;
}

/*
void Animal_Node::display_test() const
{
	if (animal_ptr)
		animal_ptr->display();

	cout << "This node has left child?: ";
	if (left)
	{
		cout << "Yes ";
		if (left_isRed)
			cout << "Red\n";
		else
			cout << "Black\n";
	}
	else
		cout << "No\n";

	cout << "This node has right child?: ";
	if (right)
	{
		cout << "Yes ";
		if (right_isRed)
			cout << "Red\n";
		else
			cout << "Black\n";
	}
	else
		cout << "No\n";
}*/

	



// Client Pet class
Pet::Pet(): root(nullptr)
{}

Pet::~Pet()
{}

Pet::Pet(const Pet & src): root(nullptr)
{
	Empty_List empty;
	try
	{
		if (!src.root)
			throw(empty);

		deepcopy(root, src.root);
	}

	catch (Empty_List)
	{
		cerr << "Object to be copied from is empty.\n";
	}
}

Pet & Pet::operator=(const Pet & src)
{
	Empty_List empty;
	try
	{
		if (!src.root)
			throw(empty);

		if (root) // deallocating first before deepcopy.
			root.reset();

		deepcopy(root, src.root);
	}

	catch (Empty_List)
	{
		cerr << "Object to be copied from is empty.\n";
	}

	return *this;
}

int Pet::deepcopy(node & root, const node & src_root)
{
	int i = 0;

	if (!src_root) // at the end
		return i;

	// create the node
	unique_ptr<Animal_Node> temp(new Animal_Node(*src_root));
	root = move(temp);

	++i;

	i = deepcopy(root->get_left(), src_root->get_left()) + deepcopy(root->get_right(), src_root->get_right());

	return i;

}

bool Pet::insert(const Animal & src) // RED-BLACK TREES
{
	if (!root) // no nodes yet
	{
		unique_ptr<Animal_Node> temp(new Animal_Node(src));
		root = move(temp);
		return true;
	}

	return insert(root, src);
}

bool Pet::insert(node & root, const Animal & src)
{
	if (!root) // at the end
	{
		unique_ptr<Animal_Node> temp(new Animal_Node(src));
		root = move(temp);
		return true;
	}


	if (root->left_red() && root->right_red()) // check if the current node/root has 3 pieces of data.
	{
		// push and split.

		root->set_left_red(false);
		root->set_right_red(false);
	}

	// root->compare(src) does root's animal name - src.

	if (root->compare(src) > 0) // to be added is less than the current node.
	{
		if (root->get_left()) // see if the node we are traversing to exists.
		{
			if (root->get_left()->left_red() && root->get_left()->right_red()) // if there is 3 pieces of data on the left node, split
			{
				root->set_left_red(true); // pushing up

				root->get_left()->set_left_red(false); // splitting
				root->get_left()->set_right_red(false);
			}


			if (root->left_red()) // checking if we need to do some rotations.
			{	
				if (root->get_left()->compare(src) > 0) // if the current node's left is the middle.
				{
					// perform the rotation
					if (!root->get_right())
					{
						unique_ptr<Animal_Node> new_right(new Animal_Node(*root)); // copy the current node and set it to the right
						root->set_right(new_right);
						root->set_right_red(true);

						*root = *(root->get_left()); // overwrite the current node to the node that is to be inserted
						root->get_left().reset(); // delete the node so that the new node can be inserted later in the code
					}
				}

				else // the data being inserted is the middle data
				{
					if (!root->get_left())
					{
						unique_ptr<Animal_Node> new_right(new Animal_Node(*root)); // copy the current node and set it to the right.
						root->set_right(new_right);
						root->set_right_red(true);

						unique_ptr<Animal_Node> new_middle(new Animal_Node(src));
						*root = *new_middle;
						return true;
					}
				}
			}

		}


		if (!root->get_left()) // if the left is null, make sure the red flag is set before adding.
		{
			root->set_left_red(true);
		}

		return insert(root->get_left(), src);
	}

	if (root->compare(src) <= 0) // if the pet being inserted is more than the current node.
	{
		if (root->get_right()) // see if the node we are traversing to exists.
		{
			if (root->get_right()->left_red() && root->get_right()->right_red()) // if there is 3 pieces of data on the right node, split
			{
				root->set_right_red(true); // pushing up

				root->get_right()->set_left_red(false); // splitting
				root->get_right()->set_right_red(false);
			}

			if (root->right_red()) // checking if we need to do some rotations.
			{	
				if (root->get_right()->compare(src) <= 0) // if the current node's right child is the middle data.
				{
					// perform the rotation
					if (!root->get_left())
					{
						unique_ptr<Animal_Node> new_left(new Animal_Node(*root)); // copy the current node and set it to the left
						root->set_left(new_left);
						root->set_left_red(true);

						*root = *(root->get_right()); // overwrite the current node to whatever the right node is.
						root->get_right().reset(); // deallocate the right node.

						/*
						unique_ptr<Animal_Node> new_right(new Animal_Node(src));
						*(root->get_right()) = *new_right;
						*/
					}
				}

				else // if the node to be inserted is the middle data.
				{
					if (!root->get_left())
					{
						unique_ptr<Animal_Node> new_left(new Animal_Node(*root)); // copy the current node 
						root->set_left(new_left);
						root->set_left_red(true);

						unique_ptr<Animal_Node> new_middle(new Animal_Node(src));
						*root = *new_middle; 
						return true;
					}
				}
			}
		}

		if (!root->get_right()) // if the right is null, make sure the red flag is set before adding.
		{
			root->set_right_red(true);
		}

		return insert(root->get_right(), src);
	}

	else // something went wrong.
		return false;
}


bool Pet::remove(const Animal_Node & src)
{

}

int Pet::display() const
{
	if (!root) // empty
		return 0;

	return display(root);
}

int Pet::display(const node & root) const
{
	int i = 0;

	if (!root) // at the end
		return 0;

	// Do in order traversal

	root->display();
	/*
	cout << "This node has left child?: ";
	if (root->get_left())
	{
		cout << "Yes ";
		if (root->left_red())
			cout << "Red\n";
		else
			cout << "Black\n";
	}
	else
		cout << "No\n";

	cout << "This node has right child?: ";
	if (root->get_right())
	{
		cout << "Yes ";
		if (root->right_red())
			cout << "Red\n";
		else
			cout << "Black\n";
	}
	else
		cout << "No\n";
		*/



	++i;

	i = i + display(root->get_left());

	/*
	root->display();
	cout << "This node has left red child?: " << root->left_red() << endl;
	cout << "This node has right red child?: " << root->right_red() << endl;
	++i;
	*/

	i = i + display(root->get_right());

	return i;
}

int Pet::weight_filter(float weight) const
{
	if (!root) // empty
		return 0;

	return weight_filter(root, weight);
}

int Pet::weight_filter(const node & root, float weight) const
{
	int i = 0;

	if (!root)
		return i;


	i = i + weight_filter(root->get_left(), weight);

	if (root->weight_filter(weight)) // do the weight comparison.
	{
		root->display();
		++i;
	}

	i = i + weight_filter(root->get_right(), weight);
	
	return i;
}

int Pet::dogs_filter() const
{
	if (!root)
		return 0;

	return dogs_filter(root);
}

int Pet::dogs_filter(const node & root) const
{
	int i = 0;

	if (!root)
		return i;
	
	i = i + dogs_filter(root->get_left());

	if (root->is_Dog())
		root->display();
	
	i = i + dogs_filter(root->get_right());

	return i;
}

int Pet::cats_filter() const // only show cats
{
	if (!root)
		return 0;

	return cats_filter(root);
}

int Pet::cats_filter(const node & root) const
{
	int i = 0;

	if (!root)
		return i;
	
	i = i + cats_filter(root->get_left());

	if (root->is_Cat())
		root->display();
	
	i = i + cats_filter(root->get_right());

	return i;

}

int Pet::fish_filter() const // only show fish
{
	if (!root)
		return 0;

	return fish_filter(root);
}

int Pet::fish_filter(const node & root) const
{
	int i = 0;

	if (!root)
		return i;
	
	i = i + fish_filter(root->get_left());

	if (root->is_Fish())
		root->display();
	
	i = i + fish_filter(root->get_right());

	return i;
}

