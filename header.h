#include <iostream>
#include <cstring>
#include <string>
#include <cctype>
#include <vector>
#include <list>
#include <exception>
#include <memory>
using namespace std;

/* Brian Le

   This file contains all the classes and their prototypes. Non Member Functions are also prototyped here.
   */

class Animal
{
	public:
		Animal();
		Animal(const string & name, const string & breed, float weight, int level_of_care, bool temperament, const string & physical_characteristics,\
				const string & history);
		virtual ~Animal();
		
		virtual void display() const = 0; // all derives must have this implemented. Some need to print different infos depending on the pet type.

		virtual bool weight_check(float) const; // checks if the level of care the user wants matches the animal's care requirements.
		virtual bool temperament_check(bool) const; // comparing 2 bools
		virtual bool care_check(int) const; // check level of care. Level of care that the user wants to find will show pets of less than or equal care.

		/* overloaded operators
		bool operator< (const Animal & src) const; // op1 = this object, op2 = src
		bool operator<= (const Animal & src) const;
		bool operator> (const Animal & src) const;
		bool operator>= (const Animal & src) const;
		bool operator== (const Animal & src) const;
		*/

		int name_compare(const Animal & src) const;	




	protected:

		string name;
		string breed; // breed for cats and dogs, species for fishes
		int level_of_care; // 1 = animal is independent, 2 = require some attention and care, 3 = requires a lot of care and attention
		float weight;
		bool temperament; // true = aggressive, false = passive
		string physical_characteristics;
		string history; // info can be written in here
};

class Animal_Node // We are going to do a Red-Black Tree
{
	public:
		Animal_Node();
		Animal_Node(const Animal_Node & src);
		Animal_Node(const Animal & src);
		~Animal_Node();

		Animal_Node & operator=(const Animal_Node & src);

		typedef unique_ptr<Animal_Node> node;

		void set_left(node & new_left);
		void set_right(node & new_right);
		node & get_left();
		node & get_right();
		bool left_red() const; // getters for the flags
		bool right_red() const;
		bool set_left_red(bool);
		bool set_right_red(bool);
		void display() const;
		bool weight_filter(float weight) const;
		
		bool is_Dog() const;
		bool is_Cat() const;
		bool is_Fish() const;
		//void display_test() const; function deprecated for debugging purposes.


		int compare(const Animal & src);

	private:

		Animal * animal_ptr;
		node left;
		node right;
		bool left_isRed;
		bool right_isRed;
};

class Dog: public Animal
{
	public:
		Dog();
		Dog(int id, const string & name, const string & breed, float weight, int level_of_care, bool temperament, const string & physical_characteristics,\
				const string & history);
		~Dog();
		bool operator== (const Dog & src) const;

		void display() const;

		bool weight_check(float) const; // check the weight of the dog.
		bool temperament_check(bool) const; // comparing 2 bools
		bool care_check(int) const; // check level of care

	private:
		int microchip_id;
};

class Cat: public Animal
{
	public:
		Cat();
		Cat(const string & personality, const string & name, const string & breed, float weight, int level_of_care, bool temperament, const string & physical_characteristics,\
				const string & history);
		~Cat();

		void display() const;

		bool temperament_check(bool) const; // comparing 2 bools
		bool care_check(int) const; // check level of care
		bool weight_check(float) const; // check the weight of the dog.



	private:
		string personality;

};

class Fish: public Animal
{
	public:
		Fish();
		Fish(const string & species, const string & compatibility, const string & name, const string & breed,\
				float weight, int level_of_care, bool temperament, const string & physical_characteristics,\
				const string & history);
		~Fish();

		void display() const;

		void display_compatibility() const; // some fishes are aggressive towards other fish. Will need to downcast/RTTI.
		bool weight_check(float) const; // check the weight of the dog.
		bool temperament_check(bool) const; // comparing 2 bools
		bool care_check(int) const; // check level of care

	private:
		string species;
		string compatibility;
};


// Client Class
class Pet
{
	public:
		Pet();
		Pet(const Pet & src); // copy constructor.
		Pet & operator= (const Pet & src);
		~Pet();

		bool insert(const Animal & src); // A dog, cat, and fish "is a" Animal + more.
		bool remove(const Animal_Node & src);
		int display() const; // show all pets in database.

		int weight_filter(float weight) const; // only show pets by weight. Fishes will not be displayed.
		int dogs_filter() const; // only show dogs.
		int cats_filter() const; // only show cats
		int fish_filter() const; // only show fish

		typedef unique_ptr<Animal_Node> node;

	private:
		bool insert(node & root, const Animal & src);
		int deepcopy(node & root, const node & src_root);
		bool insert(node & root, const Animal_Node & src);
		int display(const node & root) const;
		int weight_filter(const node & root, float weight) const;
		int dogs_filter(const node & root) const;
		int cats_filter(const node & root) const;
		int fish_filter(const node & root) const;


		node root;
};

int menu();
void backend(Pet & mypets);
void test_list(Pet & mypets);
